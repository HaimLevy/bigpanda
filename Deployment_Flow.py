import os
import sys
import tarfile
import urllib.request
import subprocess
import docker
import time


def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)

    except OSError as e:
        print('Error: Creating directory. ' + directory + e)


def downloadAndExtractToPath(picsURL, directory, fileName):
    try:
        urllib.request.urlretrieve(picsURL + fileName, filename=fileName)
        tar = tarfile.open(fileName)
        tar.extractall(path=directory)
        tar.close()
        if os.path.exists(fileName):
            try:
                os.remove(fileName)
            except:
                raise BaseException
                print("Deletion failed : " + str(e))
    except BaseException as e:
        print("Download failed : " + str(e))


def buildImage(images):
    client = docker.from_env()
    try:
        for image in images:
            client.images.build(path=image['path'], tag=image['name'])
    except BaseException as e:
        print('Building new Images failed,' + str(e))


def subprocess_cmd(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    proc_stdout = process.communicate()[0].strip()
    return proc_stdout


def healthCehck(healthCehckURL, dockerComposePath):
    try:
        response = urllib.request.urlopen(healthCehckURL)
        status_code = response.status
        print('Status Code: ' + str(status_code))
        if status_code != 200:
            raise Exception

    except Exception:
        print('App is not healthy, status code: ' + str(status_code))
        os.chdir(dockerComposePath)
        subprocess_cmd('docker-compose down')
        sys.exit()

    else:
        print('App is healthy')


def main():
    dockerComposePath = './app+db'
    directory = './app+db/public/images'
    picsURL = 'https://s3.eu-central-1.amazonaws.com/devops-exercise/'
    fileName = 'pandapics.tar.gz'
    images = [{'name': 'app-image', 'path': './app+db'}, {'name': 'mongo-image', 'path': './app+db/db/'}]
    cmd = 'docker-compose up -d'
    healthCehckURL = 'http://localhost:3000/health'

    createFolder(directory)
    downloadAndExtractToPath(picsURL, directory, fileName)
    buildImage(images)
    os.chdir(dockerComposePath)
    subprocess_cmd(cmd)
    time.sleep(30)
    healthCehck(healthCehckURL, dockerComposePath)


if __name__ == "__main__":
    main()

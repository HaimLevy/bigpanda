# BigPanda Exercise for Ops Engineers.

> Run 'pip install docker', in order to install docker library (all the other used libraries are standard/builtin)

> Download the repo files from this GIT

> Run the script 'Deployment_Flow.py' using python 3.X
______________________________________________________________________________________________________________________________________________________________
The 'Deployment_Flow.py' script is located outside the app+db folder, therefore it is crucial it will stays that way.										 
Download the 'Master' from "https://github.com/bigpandaio/ops-exercise" to 'app+db' folder > put the 'Deployment_Flow.py' at the parent folder and run it.	  
______________________________________________________________________________________________________________________________________________________________

# Folder structure:

*├───.idea  
*└───app+db  
    *  ├───bin  
    *  ├───db  
    *  ├───public  
    *  │   └───stylesheets  
    *  ├───routes  
    *  ├───setup  
    *  └───views  
  
______________________________________________________________________________________________________________________________________________________________  
 At the end of the script, if the system is healthy, you will get the next message:
 > Status Code: 200  
 > App is healthy  

In any other case, the script will fail itselfs and stop/shutdown all docker containers.